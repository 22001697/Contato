//
//  ViewController.swift
//  Contato
//
//  Created by COTEMIG on 11/08/22.
//

import UIKit

class Contato: Codable {
    var nome: String
    var email: String
    var endereco: String
    var telefone: String
    
    init(nome:String, email:String, endereco:String, telefone:String) {
        self.nome = nome
        self.email = email
        self.endereco = endereco
        self.telefone = telefone
    }
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos:[Contato] = []
    
    var userDefaults = UserDefaults.standard
    let listaDeContatosKey = "ListaDeContatos"
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func delete(rowIndexPathAt indexPath:IndexPath) -> UIContextualAction{
      
        let action = UIContextualAction(style: .destructive, title: "Excluir"){[weak self] (_,_,_) in
            guard let self = self else{return};
            let alert = UIAlertController(title: "Excluir", message: "Certeza que deseja o contato", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title:"Cancelar", style: .default, handler: nil))
            alert.addAction(UIAlertAction(title: "Excluir", style: .destructive, handler:{ action in
                self.excluirContato(index: indexPath.row)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        return action
        
    }
    
    func edit(rowIndexPathAt indexPath: IndexPath) -> UIContextualAction {
       let action = UIContextualAction(style: .normal, title: "Edit") { [weak self] (_, _, _) in
        let alert = UIAlertController(title: "Do you want to edit? ", message: "You could do so when we implement this functionality", preferredStyle: .alert);            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self?.present(alert, animated: true)
        }

return action
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = self.delete(rowIndexPathAt: indexPath)
        let edit = self.edit(rowIndexPathAt: indexPath)
        let swipe = UISwipeActionsConfiguration(actions: [delete, edit])
        return swipe
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let celula = tableview.dequeueReusableCell(withIdentifier: "MinhaCelula",for: indexPath) as! MyCell
        let contato = listaDeContatos[indexPath.row]
        
        celula.nome.text = contato.nome
        celula.email.text = contato.email
        celula.endereco.text = contato.endereco
        celula.telefone.text = contato.telefone
        
        return celula
    }
    

    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.dataSource = self
        tableview.delegate = self
        
        let listaData = userDefaults.value(forKey: listaDeContatosKey) as! Data

        do{
            let listaContatos = try JSONDecoder().decode([Contato].self, from: listaData)
            listaDeContatos = listaContatos
            tableview.reloadData()
        }catch{
            print("Erro em converter em dados:\(error.localizedDescription)")
        }
        
    }

    func salvarContatosLocal(){
        do{
            let dataLista = try JSONEncoder().encode(listaDeContatos)
            userDefaults.setValue(dataLista, forKey: listaDeContatosKey)
        }catch{
            print("Erro ao salvar na memoria local:\(error.localizedDescription)")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "abrirDetalhes"{
        let detalhesViewController = segue.destination as! DetalhesViewController
        let index = sender as! Int
        let contato = listaDeContatos[index]
        detalhesViewController.index = index
        detalhesViewController.contato = contato
        detalhesViewController.delegate = self
        detalhesViewController.contatoDelegate = self
        } else if segue.identifier == "criarContato" {
            let contatoViewController = segue.destination as! ContatoViewController
            contatoViewController.delegate = self
        }
}

}

extension ViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "abrirDetalhes", sender: indexPath.row)
    }
}

extension ViewController: ContatoViewControllerDelegate {
    func salvarNovoContato(contato:Contato){
        listaDeContatos.append(contato)
        tableview.reloadData()
        salvarContatosLocal()
    }
    
    func editarContato(){
        tableview.reloadData()
        salvarContatosLocal()
    }
}

extension ViewController: DetalhesViewControllerDelegate{
    func excluirContato(index: Int) {
        listaDeContatos.remove(at: index)
        tableview.reloadData()
        salvarContatosLocal()
    }
}
