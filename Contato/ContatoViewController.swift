
import UIKit
protocol ContatoViewControllerDelegate {
    func salvarNovoContato(contato:Contato)
    func editarContato()
}

class ContatoViewController: UIViewController {

    @IBOutlet weak var nomeField: UITextField!
    @IBOutlet weak var numeroField: UITextField!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var enderecoField: UITextField!
    
    
    public var delegate: ContatoViewControllerDelegate?
    public var contato: Contato?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        nomeField.text = contato?.nome
        numeroField.text = contato?.telefone
        emailField.text = contato?.email
        enderecoField.text =  contato?.endereco
        
        if contato == nil {
            title = "Novo contato"
        }else{
            title = "Editar contato"
        }
    }
    
    
    @IBAction func salvarContato(_ sender: Any) {
        if contato == nil {
            let contato = Contato(nome: nomeField?.text ?? "",
                                  email: emailField?.text ?? "",
                                  endereco: enderecoField?.text ?? "",
                                  telefone: numeroField?.text ?? "")
            
            delegate?.salvarNovoContato(contato: contato)
        }else{
            contato?.nome = nomeField?.text ?? ""
            contato?.telefone = numeroField?.text ?? ""
            contato?.email = emailField?.text ?? ""
            contato?.endereco = enderecoField?.text ?? ""
       
            delegate?.editarContato()
        }
        navigationController?.popViewController(animated: true)
    }
        
}

